package com.github.client2.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * @author dellll
 */
@Configuration
public class WebMvcConfig extends WebMvcConfigurationSupport {


  /**
   * 这个地方要重新注入一下资源文件，不然不会注入资源的，也没有注入requestHandlerMappping,相当于xml配置的
   *
   * @param registry
   */
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("swagger-ui.html")
        .addResourceLocations("classpath:/META-INF/resources/");
    registry.addResourceHandler("/webjars*")
        .addResourceLocations("classpath:/META-INF/resources/webjars/");
    registry.addResourceHandler("/**")
        .addResourceLocations("classpath:/static/");
    super.addResourceHandlers(registry);
  }


  /**
   * 注册ViewController
   *
   * @param registry
   */
  @Override
  public void addViewControllers(ViewControllerRegistry registry) {
    registry.addViewController("/").setViewName("/swagger-ui.html");
  }

  /**
   * 设置返回数据头
   *
   * @return
   */
  @Bean
  public MappingJackson2HttpMessageConverter jackson2HttpMessageConverter() {
    MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();
    List supportedMediaTypes = new ArrayList();
    supportedMediaTypes.add(new MediaType("text", "plain"));
    supportedMediaTypes.add(new MediaType("application", "json"));
    jsonMessageConverter.setSupportedMediaTypes(supportedMediaTypes);
    return jsonMessageConverter;
  }

  @Override
  public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
    converters.add(jackson2HttpMessageConverter());
  }

}
